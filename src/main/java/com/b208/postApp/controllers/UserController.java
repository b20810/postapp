package com.b208.postApp.controllers;

import com.b208.postApp.exceptions.UserException;
import com.b208.postApp.models.User;
import com.b208.postApp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin

public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/users/register")
    public ResponseEntity<Object> createUser(@RequestBody Map<String,String> body) throws UserException {

        String username = body.get("username");

        if(!userService.findByUsername(username).isEmpty()){
            throw new UserException("Username already exists");
        } else {
            String password = body.get("password");
            if(password.length() < 8){
                return new ResponseEntity<>("Please add an 8 or more character password",HttpStatus.OK);
            }
            String encodedPassword = new BCryptPasswordEncoder().encode(password);

            User newUser = new User(username,encodedPassword);
            userService.createUser(newUser);

            return new ResponseEntity<>("User registered successfully",HttpStatus.CREATED);
        }

    }

    @GetMapping("/users")
    public ResponseEntity<Object> getUsers(){

        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@RequestHeader(value = "Authorization") String stringToken, @PathVariable Long id, @RequestBody User user){

        return userService.updateUser(stringToken,id,user);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long id){

        userService.deleteUser(id);

        return new ResponseEntity<>("User deleted successfully!", HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getOneUser(@PathVariable Long id){

        return new ResponseEntity<>(userService.getOneUser(id),HttpStatus.OK);
    }

    @GetMapping("/doesUserExist/{id}")
    public ResponseEntity<Object> doesUserExist(@PathVariable Long id){

        return new ResponseEntity<>(userService.doesUserExist(id), HttpStatus.OK);
    }

    @GetMapping("/numberOfUsers")
    public ResponseEntity<Long> countUsers(){

        return new ResponseEntity<>(userService.countUsers(),HttpStatus.OK);
    }



}
