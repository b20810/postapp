package com.b208.postApp.repositories;


import com.b208.postApp.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// An interface contains behaviors or methods that a class should contain.
// An interface marked as @Repository means that the interface would contain or contains methods for database manipulation
// By extending CrudRepository, PostRepository has inherited pre-defined mathods for creating, retrieving, updating and deleting records.

@Repository
public interface PostRepository extends CrudRepository<Post,Object> {
}
