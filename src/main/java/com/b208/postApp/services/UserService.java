package com.b208.postApp.services;

import com.b208.postApp.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {

    void createUser(User user);
    Iterable<User> getUsers();
    ResponseEntity updateUser(String stringToken, Long id, User user);
    void deleteUser(Long id);
    Object getOneUser(Long id);
    String doesUserExist(Long id);
    Long countUsers();
    Optional<User> findByUsername(String username);

}
