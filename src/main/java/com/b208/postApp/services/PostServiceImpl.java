package com.b208.postApp.services;

import com.b208.postApp.config.JwtToken;
import com.b208.postApp.models.Post;
import com.b208.postApp.models.User;
import com.b208.postApp.repositories.PostRepository;
import com.b208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

// @Service is added so that springboot would recognize that business logic is implement in this layer
@Service
public class PostServiceImpl implements PostService {

    // We're going to create an instance of the postRepository that is persistent within our springboot app
    // This is so we can use the pre-defined methods for database manipulation for our table.
    @Autowired
    private PostRepository postRepository;
    @Autowired
    JwtToken jwtToken;
    @Autowired
    private UserRepository userRepository;
    public String createPost(String stringToken, Post post){
        // Get the username from the token using jwtToken getUsernameFromToken method.
        // Then, pass the username:
        // Get the user's details from our table, using our userRepository findByUsername method.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        // Check if you can get the user from the table
        // System.out.println(author.getUsername());

        // When createPost is used from our services, we will be able to pass a Post class object and then, using the .save() method of our postRepository we will be able to insert a new row into our posts table.

        // System.out.println(post.getTitle());
        // System.out.println(post.getContent());
        if(post.getTitle() == null || post.getTitle().length() == 0 || post.getContent() == null || post.getContent().length() == 0){
            return "Invalid input. Please complete the form";
        }
        Post newPost = new Post();
        // post.getTitle() and post.getContent() is from our request body passed as post.
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
        return "Post Created Successfully";

    }

    public Iterable<Post> getPosts(){
        // return all records from our table
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(String stringToken,Long id, Post post){

        if(post.getTitle() == null || post.getTitle().length() == 0 || post.getContent() == null || post.getContent().length() == 0){
            return new ResponseEntity<>("Invalid input. Please complete the form", HttpStatus.BAD_REQUEST);
        }

        // post parameter here is actually the request object passed from the controller.
        // because post parameter is actually the request body converted as a post class object, it has the methods from a post class object.
        // Therefore, getTitle() will actually get the value of the title property from the request body.

        // System.out.println("This is the id passed as path variable");
        // System.out.println(id);

        // findById() retrieves a record that matches the passed id
        Post postForUpdating = postRepository.findById(id).get();

        // After getting the user of the post, get the username of the user.
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        // System.out.println(authenticatedUser.equals(postAuthor));
        // check if the user trying to update is the same as the author.
        if(authenticatedUser.equals(postAuthor)){
            // System.out.println("This is the found data using the id");
            // System.out.println(postForUpdating.getTitle());

            // System.out.println("This is the request body passed form the controller");
            // System.out.println(post.getTitle());

            // The found post title will be updated with the title of the request body as post
            postForUpdating.setTitle(post.getTitle());
            // The found post content will be updated with the content of the request body as post
            postForUpdating.setContent(post.getContent());
            // save the updates to our data.
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post Updated Successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("You are not authorized to updated this post.", HttpStatus.UNAUTHORIZED);
    }

    public ResponseEntity deletePost(String stringToken, Long id){

        Post postForDeleting = postRepository.findById(id).get();
        String postAuthor = postForDeleting.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        // check if the user trying to update is the same as the author.
        if(authenticatedUser.equals(postAuthor)){
            // Delete the record that matches the passed id
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post Deleted Successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
    }

    public ResponseEntity getMyPosts(String stringToken){

        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        Set<Post> myPosts = userRepository.findByUsername(authenticatedUser).getPosts();

        return new ResponseEntity(myPosts, HttpStatus.OK);
    }
}
