package com.b208.postApp.services;

import com.b208.postApp.config.JwtToken;
import com.b208.postApp.models.User;
import com.b208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;
    public void createUser(User user){

        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public Iterable<User> getUsers(){

        return userRepository.findAll();
    }

    public ResponseEntity updateUser(String stringToken, Long id, User user){

        User userForUpdating = userRepository.findById(id).get();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(user.getUsername())){
            userForUpdating.setUsername(user.getUsername());
            userForUpdating.setPassword(user.getPassword());

            userRepository.save(userForUpdating);

            return new ResponseEntity<>("username and password updated successfully!", HttpStatus.OK);
        }
        return new ResponseEntity<>("You are not authorized to update User", HttpStatus.UNAUTHORIZED);
    }

    public void deleteUser(Long id){

        userRepository.deleteById(id);
    }

    public Object getOneUser(Long id){

        // System.out.println(userRepository.findById(id).get());
        return userRepository.findById(id).get();
    }

    public String doesUserExist(Long id){

        if(userRepository.existsById(id)){
            return "User found";
        }
        return "User Not Found";
    }

    public Long countUsers(){

        return userRepository.count();
    }

}
